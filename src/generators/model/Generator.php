<?php

namespace CuiFox\gii\generators\model;

use CuiFox\yii\db\ActiveRecord;
use yii\base\NotSupportedException;
use yii\base\InvalidConfigException;
use yii\db\Schema;
use yii\gii\CodeFile;
use yii\helpers\Inflector;
use yii\db\Exception;
use Yii;

class Generator extends \yii\gii\generators\model\Generator
{
    public $baseClass = ActiveRecord::class;

    /**
     * @return array|CodeFile[]
     * @throws NotSupportedException
     */
    public function generate()
    {
        $files = [];
        $relations = $this->generateRelations();
        $db = $this->getDbConnection();
        foreach ($this->getTableNames() as $tableName) {
            // model :
            $modelClassName = $this->generateClassName($tableName);
            $queryClassName = ($this->generateQuery) ? $this->generateQueryClassName($modelClassName) : false;
            $tableRelations = isset($relations[$tableName]) ? $relations[$tableName] : [];
            $tableSchema = $db->getTableSchema($tableName);
            $params = [
                'tableName' => $tableName,
                'className' => $modelClassName,
                'queryClassName' => $queryClassName,
                'tableSchema' => $tableSchema,
                'properties' => $this->generateProperties($tableSchema),
                'labels' => $this->generateLabels($tableSchema),
                'rules' => $this->generateRules($tableSchema),
                'relations' => $tableRelations,
                'relationsClassHints' => $this->generateRelationsClassHints($tableRelations, $this->generateQuery),
            ];
            $files[] = new CodeFile(Yii::getAlias('@' . str_replace('\\', '/',
                        $this->ns)) . '/' . $modelClassName . '.php', $this->render('model.php', $params));

            // query :
            if ($queryClassName) {
                $params['className'] = $queryClassName;
                $params['modelClassName'] = $modelClassName;
                $files[] = new CodeFile(Yii::getAlias('@' . str_replace('\\', '/',
                            $this->queryNs)) . '/' . $queryClassName . '.php', $this->render('query.php', $params));
            }
        }

        return $files;
    }

    /**
     * Generates the properties for the specified table.
     * @param \yii\db\TableSchema $table the table schema
     * @return array the generated properties (property => type)
     * @since 2.0.6
     */
    protected function generateProperties($table)
    {
        $properties = [];
        foreach ($table->columns as $column) {
            switch ($column->type) {
                case Schema::TYPE_SMALLINT:
                case Schema::TYPE_INTEGER:
                case Schema::TYPE_BIGINT:
                case Schema::TYPE_TINYINT:
                    $type = 'int';
                    break;
                case Schema::TYPE_BOOLEAN:
                    $type = 'bool';
                    break;
                case Schema::TYPE_FLOAT:
                case Schema::TYPE_DOUBLE:
                case Schema::TYPE_DECIMAL:
                case Schema::TYPE_MONEY:
                    $type = 'float';
                    break;
                case Schema::TYPE_DATE:
                case Schema::TYPE_TIME:
                case Schema::TYPE_DATETIME:
                case Schema::TYPE_TIMESTAMP:
                case Schema::TYPE_JSON:
                    $type = 'string';
                    break;
                default:
                    $type = $column->phpType;
            }
            if ($column->allowNull) {
                $type .= '|null';
            }
            $properties[$column->name] = [
                'type' => $type,
                'name' => $column->name,
                'comment' => $this->filterString($column->comment),
            ];
        }

        return $properties;
    }

    /**
     * @param string $string
     * @return string
     */
    private function filterString($string = '')
    {
        if (empty($string)) {
            return '';
        }
        $list = preg_split("/，|[;,\s]+/", trim($string));
        return $list[0];
    }

    /**
     * Generates the attribute labels for the specified table.
     * @param \yii\db\TableSchema $table the table schema
     * @return array the generated attribute labels (name => label)
     */
    public function generateLabels($table)
    {
        $labels = [];
        foreach ($table->columns as $column) {
            if ($this->generateLabelsFromComments && !empty($column->comment)) {
                $labels[$column->name] = $this->filterString($column->comment);
            } elseif (!strcasecmp($column->name, 'id')) {
                $labels[$column->name] = 'ID';
            } else {
                $label = Inflector::camel2words($column->name);
                if (!empty($label) && substr_compare($label, ' id', -3, 3, true) === 0) {
                    $label = substr($label, 0, -3) . ' ID';
                }
                $labels[$column->name] = $label;
            }
        }

        return $labels;
    }

    /**
     * @return array
     * @throws NotSupportedException
     */
    protected function generateRelations()
    {
        if ($this->generateRelations === self::RELATIONS_NONE) {
            return [];
        }

        $db = $this->getDbConnection();
        $relations = [];
        $schemaNames = $this->getSchemaNames();
        foreach ($schemaNames as $schemaName) {
            foreach ($db->getSchema()->getTableSchemas($schemaName) as $table) {
                $className = $this->generateClassName($table->fullName);
                foreach ($table->foreignKeys as $refs) {
                    $refTable = $refs[0];
                    $refTableSchema = $db->getTableSchema($refTable);
                    if ($refTableSchema === null) {
                        // Foreign key could point to non-existing table: https://github.com/yiisoft/yii2-gii/issues/34
                        continue;
                    }
                    unset($refs[0]);
                    $fks = array_keys($refs);
                    $refClassName = $this->generateClassName($refTable);

                    // Add relation for this table
                    $link = $this->generateRelationLink(array_flip($refs));
                    $relationName = $this->generateRelationName($relations, $table, $fks[0], false);
                    $relations[$table->fullName][$relationName] = [
                        "return \$this->hasOne($refClassName::class, $link);",
                        $refClassName,
                        false,
                    ];

                    // Add relation for the referenced table
                    $hasMany = $this->isHasManyRelation($table, $fks);
                    $link = $this->generateRelationLink($refs);
                    $relationName = $this->generateRelationName($relations, $refTableSchema, $className, $hasMany);
                    $relations[$refTableSchema->fullName][$relationName] = [
                        "return \$this->" . ($hasMany ? 'hasMany' : 'hasOne') . "($className::class, $link);",
                        $className,
                        $hasMany,
                    ];
                }

                foreach ($table->columns as $column) {
                    $relation = null;
                    if ($column->comment) {
                        $result = preg_match("/[^\[]*\[([^\]]*)\].*/", $column->comment, $matches);
                        if ($result) {
                            $relation = $matches[1];
                            if ($relation) {
                                $array = explode(',', $relation);
                                $refs = [
                                    $db->tablePrefix . $array[0],
                                    $column->name => $array[1],
                                ];

                                $refTable = $refs[0];
                                $refTableSchema = $db->getTableSchema($refTable);
                                if ($refTableSchema === null) {
                                    // Foreign key could point to non-existing table: https://github.com/yiisoft/yii2-gii/issues/34
                                    continue;
                                }
                                unset($refs[0]);
                                $fks = array_keys($refs);
                                $refClassName = $this->generateClassName($refTable);

                                // Add relation for this table
                                $link = $this->generateRelationLink(array_flip($refs));
                                $relationName = $this->generateRelationName($relations, $table, $fks[0], false);
                                $relations[$table->fullName][$relationName] = [
                                    "return \$this->hasOne($refClassName::class, $link);",
                                    $refClassName,
                                    false,
                                ];

                                // Add relation for the referenced table
                                $hasMany = $this->isHasManyRelation($table, $fks);
                                $link = $this->generateRelationLink($refs);
                                $relationName = $this->generateRelationName($relations, $refTableSchema, $className,
                                    $hasMany);
                                $relations[$refTableSchema->fullName][$relationName] = [
                                    "return \$this->" . ($hasMany ? 'hasMany' : 'hasOne') . "($className::class, $link);",
                                    $className,
                                    $hasMany,
                                ];
                            }
                        }
                    }
                }

                if (($junctionFks = $this->checkJunctionTable($table)) === false) {
                    continue;
                }

                $relations = $this->generateManyManyRelations($table, $junctionFks, $relations);
            }
        }

        if ($this->generateRelations === self::RELATIONS_ALL_INVERSE) {
            return $this->addInverseRelations($relations);
        }

        return $relations;
    }

    /**
     * @return string[] all db schema names or an array with a single empty string
     * @throws NotSupportedException
     * @since 2.0.5
     */
    protected function getSchemaNames()
    {
        $db = $this->getDbConnection();

        if ($this->generateRelationsFromCurrentSchema) {
            if ($db->schema->defaultSchema !== null) {
                return [$db->schema->defaultSchema];
            }
            return [''];
        }

        $schema = $db->getSchema();
        if ($schema->hasMethod('getSchemaNames')) { // keep BC to Yii versions < 2.0.4
            try {
                $schemaNames = $schema->getSchemaNames();
            } catch (NotSupportedException $e) {
                // schema names are not supported by schema
            }
        }
        if (!isset($schemaNames)) {
            if (($pos = strpos($this->tableName, '.')) !== false) {
                $schemaNames = [substr($this->tableName, 0, $pos)];
            } else {
                $schemaNames = [''];
            }
        }
        return $schemaNames;
    }

    /**
     * Generates relations using a junction table by adding an extra via() or viaTable() depending on $generateViaRelationMode.
     * @param \yii\db\TableSchema the table being checked
     * @param array $fks obtained from the checkJunctionTable() method
     * @param array $relations
     * @return array modified $relations
     */
    private function generateManyManyRelations($table, $fks, $relations)
    {
        $db = $this->getDbConnection();

        foreach ($fks as $pair) {
            list($firstKey, $secondKey) = $pair;
            $table0 = $firstKey[0][0];
            $table1 = $secondKey[0][0];
            unset($firstKey[0][0], $secondKey[0][0]);
            $className0 = $this->generateClassName($table0);
            $className1 = $this->generateClassName($table1);
            $table0Schema = $db->getTableSchema($table0);
            $table1Schema = $db->getTableSchema($table1);

            // @see https://github.com/yiisoft/yii2-gii/issues/166
            if ($table0Schema === null || $table1Schema === null) {
                continue;
            }

            $link = $this->generateRelationLink(array_flip($secondKey[0]));
            $relationName = $this->generateRelationName($relations, $table0Schema, key($secondKey[0]), true);
            if ($this->generateJunctionRelationMode === self::JUNCTION_RELATION_VIA_TABLE) {
                $relations[$table0Schema->fullName][$relationName] = [
                    "return \$this->hasMany($className1::className(), $link)->viaTable('"
                    . $this->generateTableName($table->name) . "', " . $this->generateRelationLink($firstKey[0]) . ');',
                    $className1,
                    true,
                ];
            } elseif ($this->generateJunctionRelationMode === self::JUNCTION_RELATION_VIA_MODEL) {
                $foreignRelationName = null;
                foreach ($relations[$table0Schema->fullName] as $key => $foreignRelationConfig) {
                    if ($foreignRelationConfig[3] == $firstKey[1]) {
                        $foreignRelationName = $key;
                        break;
                    }
                }
                if (empty($foreignRelationName)) {
                    throw new Exception('Foreign key for junction table not found.');
                }
                $relations[$table0Schema->fullName][$relationName] = [
                    "return \$this->hasMany($className1::className(), $link)->via('"
                    . lcfirst($foreignRelationName) . "');",
                    $className1,
                    true,
                ];
            }

            $link = $this->generateRelationLink(array_flip($firstKey[0]));
            $relationName = $this->generateRelationName($relations, $table1Schema, key($firstKey[0]), true);
            if ($this->generateJunctionRelationMode === self::JUNCTION_RELATION_VIA_TABLE) {
                $relations[$table1Schema->fullName][$relationName] = [
                    "return \$this->hasMany($className0::className(), $link)->viaTable('"
                    . $this->generateTableName($table->name) . "', " . $this->generateRelationLink($secondKey[0]) . ');',
                    $className0,
                    true,
                ];
            } elseif ($this->generateJunctionRelationMode === self::JUNCTION_RELATION_VIA_MODEL) {
                $foreignRelationName = null;
                foreach ($relations[$table1Schema->fullName] as $key => $foreignRelationConfig) {
                    if ($foreignRelationConfig[3] == $secondKey[1]) {
                        $foreignRelationName = $key;
                        break;
                    }
                }
                if (empty($foreignRelationName)) {
                    throw new Exception('Foreign key for junction table not found.');
                }
                $relations[$table1Schema->fullName][$relationName] = [
                    "return \$this->hasMany($className0::className(), $link)->via('"
                    . lcfirst($foreignRelationName) . "');",
                    $className0,
                    true,
                ];
            } else {
                throw new InvalidConfigException('Unknown generateViaRelationMode ' . $this->generateJunctionRelationMode);
            }
        }

        return $relations;
    }
}