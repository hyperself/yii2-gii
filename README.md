# yii2-gii

#### 介绍
yii2-gii

#### 安装教程
```shell script
php composer.phar require cuifox/yii2-gii:*
```

#### 使用说明

```php
if (YII_ENV_DEV) {
    // ...
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['127.0.0.1', '::1', '*'],
        'generators' => [
            'model' => [
                'class' => CuiFox\gii\generators\model\Generator::class,
                'ns' => 'app\models\common',
            ],
        ],
    ];
}
```